<?php


namespace parking\request;

use parking\request\extend\RequestConfig;
use parking\request\interfaces\Request;

/**
 * 车位宝可退金额查询
 *
 * @Author zhoulongtao email1946367301
 * @DateTime 2021-03-04
 * 
 */
class ParkingQueryPayInfoRequest extends RequestConfig implements Request
{
    
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
        'default' => 'v1/pay/queryPayInfo',
        'v1' => 'v1/pay/queryPayInfo',
    ];

}



