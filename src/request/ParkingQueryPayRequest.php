<?php
/*
 * @Author: your name
 * @Date: 2022-04-27 15:40:07
 * @LastEditTime: 2022-04-27 15:51:20
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: /che/src/request/ParkingRefundRequest copy.php
 */


namespace parking\request;

use parking\request\extend\RequestConfig;
use parking\request\interfaces\Request;

/**
 * 车位宝消费订单查询
 *
 * @Author zhoulongtao email1946367301
 * @DateTime 2021-03-04
 * 
 */
class ParkingQueryPayRequest extends RequestConfig implements Request
{
    
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
        'default' => 'v1/pay/queryPay',
        'v1' => 'v1/pay/queryPay',
    ];

}



