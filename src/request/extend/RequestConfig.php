<?php
/*
 * @Author: your name
 * @Date: 2022-04-25 15:15:45
 * @LastEditTime: 2022-04-25 15:19:40
 * @LastEditors: your name
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: /che/src/request/extend/RequestConfig.php
 */

namespace parking\request\extend;

/**
 * Undocumented class
 *
 * @Author zhoulongtao email1946367301
 * @DateTime 2021-03-04
 */
class RequestConfig
{

    protected $bizContent;
    protected $apiParas = array();
    protected $apiVersion = "default";
    /**
     * @var array 版本列表
     */
    protected $methodNameList = [

    ];

    protected $sort = '';

    public function setBizContent($bizContent)
    {
        $this->bizContent = $bizContent;
        $this->apiParas["biz_content"] = $bizContent;
    }

    public function getBizContent()
    {
        return $this->bizContent;
    }

    public function getApiMethodName()
    {
        return $this->methodNameList[$this->apiVersion];
    }

    /**
     * 获取api参数
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * 设置版本号
     * @param $apiVersion
     */
    public function setApiVersion($apiVersion)
    {
        $this->apiVersion = $apiVersion;
    }

    /**
     * 获取版本号
     * @return string
     */
    public function getApiVersion()
    {
        return $this->apiVersion;
    }

    /**
     * 获取排序
     * @return string
     */
    public function getSort()
    {
        return $this->sort;
    }
}
