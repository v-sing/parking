<?php
/**
 * Created by PhpStorm.
 * User: zhoulongtao
 * Date: 2021-03-03
 * Time: 20:50
 */

namespace parking\signData;


class Des
{
    private $desKey = '';

    public function __construct($des)
    {

        $this->desKey = $des;

    }

    /**
     * 加密
     * @param $data
     * @return string
     */
    public function encrypt($data)
    {

        return base64_encode(openssl_encrypt($data, 'DES-EDE3', $this->desKey, OPENSSL_RAW_DATA));

    }

    /**
     * 解密
     * @param $data
     * @return string
     */
    public function decrypt($data)
    {
        return openssl_decrypt(base64_decode($data), 'DES-EDE3', $this->desKey, OPENSSL_RAW_DATA);
    }


}